import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    titles = df["Name"].str.extract('([A-Za-z]+)\.')
    df["Title"] = titles
    output = []
    for i in ['Miss', 'Mrs', 'Mr']:
        median_age = df.loc['Title' == i, 'Age'].median()
        missing = df.loc[(df['Title'] == i) and (df['Age'].isnull()), 'Age'].count()
        output.append((i + '.' + missing + round(median_age)))

    return output
